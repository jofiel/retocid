# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0003_dailyentry_source'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='dailyentry',
            unique_together=set([('user', 'date', 'source')]),
        ),
    ]
