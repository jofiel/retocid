# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django.conf import settings
from celery import shared_task

from datetime import datetime

from fitbit import Fitbit
from fitbit.exceptions import (BadResponse, DeleteError, HTTPBadRequest, 
        HTTPUnauthorized, HTTPForbidden, HTTPServerError, HTTPConflict, HTTPNotFound)

from reports.models import MyUserFitbit
from reports.models import DailyEntry
from reports.models import Version
from django.contrib.auth.models import User

@shared_task()
def add(x, y):
    return x + y

class UnknownArgs(Exception):
    pass

@shared_task(ignore_result=True)
def download_data(user, what, version, start=None, end=None, period=None):
    authd_client = Fitbit(settings.FITAPP_CONSUMER_KEY, settings.FITAPP_CONSUMER_SECRET, 
            resource_owner_key=user.auth_token, resource_owner_secret=user.auth_secret)
    try:
        if start and period:
            result = authd_client.time_series(what, base_date=start, period='7d')
        elif period:
            result = authd_client.time_series(what, period='7d')
        elif start and end:
            result = authd_client.time_series(what, base_date=start, end_date=end)
        else:
            raise UnknownArgs('En shared task, combinación no admitida')
    except HTTPUnauthorized:
        user.authorized = False
        user.save()
        return
    except UnknownArgs as e:
        print e
        return

    user.authorized = True
    user.save()

    for entry in result[what.replace('/', '-')]:
        date = datetime.strptime(entry['dateTime'], "%Y-%m-%d")
        steps = entry['value']
        de, created = DailyEntry.objects.update_or_create(user = user, date = date,
                source = 0, version=version, defaults={'steps':steps})

#ts = authd_client.time_series('activities/tracker/steps', 
#        base_date=datetime(2014, 11, 17),
#        end_date=datetime(2014, 11, 25))

@shared_task(ignore_result=True)
def check_week(user, version):
    download_data(user, 'activities/steps', version, period='7d')
    download_data(user, 'activities/tracker/steps', version, period='7d')

@shared_task(ignore_result=True)
def get_special():
    u = User.objects.filter(username = 'yussethr').get()
    uf = MyUserFitbit.objects.filter(user = u).get()

    v = Version.objects.filter(id = 1).get()
    download_data.delay(uf, 'activities/steps', v, start='2014-11-17', end='2014-11-23')
    download_data.delay(uf, 'activities/tracker/steps', v, start='2014-11-17', end='2014-11-23')

    v = Version.objects.filter(id = 2).get()
    download_data.delay(uf, 'activities/steps', v, start='2014-11-24', end='2014-11-30')
    download_data.delay(uf, 'activities/tracker/steps', v, start='2014-11-24', end='2014-11-30')

@shared_task(ignore_result=True)
def check_all():
    v = Version.objects.filter(active = True).get()
    for u in MyUserFitbit.objects.all():
        check_week.delay(u, v)

@shared_task(ignore_result=True)
def check_special():
    v = Version.objects.filter(id = 1).get()
    for u in MyUserFitbit.objects.all():
        download_data.delay(u, 'activities/steps', v, start='2014-11-17', end='2014-11-30')
        download_data.delay(u, 'activities/tracker/steps', v, start='2014-11-17', end='2014-11-30')

