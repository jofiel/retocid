from django.conf.urls import patterns, include, url
from django.contrib import admin
#import fitapp

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'retocid.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^fitbit/', include('fitapp.urls')),
    url(r'^my/', include('myauth.urls', namespace='my')),
    url(r'^r/', include('reports.urls')),
    url(r'^$', 'myauth.views.login_user'),
)
